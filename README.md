Weekly Game Jam 163: Tycoon


How to use:

UP/DOWN - Scroll through the letters on the right
LEFT_KEY/RIGHT_KEY - Scroll through made/saved words
SPACEBAR - Buys a letter
ENTER - Makes/Saves written word
ALL LETTER KEYS - Writes out the letter
BACK_SPACE - removes a written letter


Missing:

Checks if User entered word is an actual word (Needs API)
Read neccesary data dump from file


Missing/Desired implimentation:

Value generated from actual data on how common word is (Needs API)
Income based on how much word actually has been used the last 30-60sek (Needs API or similar)



Resources:
Java
Eclipse
Maven
Unirest
WordsAPI
RapidAPI

Photoshop