package com.mycompany.app;

import javax.swing.JFrame;



public class Main {
	
	// Initializing the window and keyListener
	public Main() {
		
		JFrame frame = new JFrame();
		Board board =  new Board();
		
		frame.add(board);
		frame.addKeyListener(board);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("AlphabetTYCOON");

		frame.pack();
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
	}

	
	
	// Starts the program
	public static void main(String[] args) {
		
	 new Main();
		
	}
	


}
