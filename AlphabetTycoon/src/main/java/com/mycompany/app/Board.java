package com.mycompany.app;

import javax.swing.JPanel;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;




public class Board extends JPanel implements Runnable, KeyListener{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1566423871037886907L;
	// Idk what this do ^
	
	

	// General variables for the program to work
	public static final int WIDTH = 665, HEIGHT = 300;
	Thread thread;

	
	
//	GetAPI api = new GetAPI();
	
	Player player =  new Player();				// The player
	String userMessage = "WRITE A WORD";		// User/error messages
	
	
	
	// Variables in charge of income
	int income = 0;
	long lastTime = System.currentTimeMillis();	// Controls how often income generates
	
	
	
	// Variables in charge of composing the word
	String wordInMaking = "";
	int [] word = new int[20];	// Word length are limited to 20
	int currentCharNr = -1;		// Controls last letter "typed"
	
	
	
	// Variables in charge of scrolling
	Boolean scrollLeft = false;
	Boolean scrollRight = false;
	int scrollStart = 0;		// Decides which word in the array is the first word to be drawn
	int whichLetter = 0;		// Which letter the user has chosen and, then also decides where the pointer is
	


	
	
	
	
	
	// Initializing things
	public Board() {
		
		super();
		
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
	
		thread = new Thread(this);
		thread.start();
	}
	
	
	
	
	
	
	
	
	// Draw all the pieces on the board
	public void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		
		//api.getAPI();
		
		
		
		// Draw borders and still symbols
		for(int i = 0; i <WIDTH; i+=15) {
			for(int j = 10; j <HEIGHT; j+=15) {
				
				if(j == 10 || (j == (2*15)+10 && i < 26*15) || j == (14*15)+10 || j == (19*15)+10) {
					g.drawString("_",i, j);
				
				} else if(i == 0 || i == 44*15 || (i == 26*15 && j < 15*15) || (i == 35*15 && j < 15*15)) {
					g.drawString("|",i, j);
				} else if (i == 2*15 && j == (1*15)+10 ) {
					g.drawString("$",i, j);
				} else if (i == 1*15 && j == (17*15)+10 && scrollLeft) {
					g.drawString("<",i, j);
				} else if (i == 43*15 && j == (17*15)+10 && scrollRight) {
					g.drawString(">",i, j);
				}
			}
		}
		
		
		
		
		// i and j for everything else that can't be easily included above
		// and so it's easier to remember which is supposed to go where
		int i = 30*15;
		int j = 25;
		
		
		
		
		// Draws all the letters and prices, with updated amounts
		for(int k = 0; k <26; k++) {
			
			if(k == 13) {
				i = 39*15;
				j = 25;
			}
			
			player.letter[k].updateString();
			g.drawString(player.letter[k].infoPrint, i, j);
			
			j += 15;
		}
		
		
		
		
		// Draws the letter selector
		if (whichLetter > 12) {
			i = 37*15;
			j = 25 + 15 * (whichLetter-13);
		} else {
			i = 28*15;
			j = 25 + 15 * whichLetter;
		}
		
		g.drawString(">", i, j);
		
		
		
		
		// Draw message to the user
		i = 13*15 - userMessage.length()*4;
		g.drawString(userMessage, i, 100);
		
		
		
		
		// Draw player money
		String playerMoney;
		if (player.money < 10) {
			playerMoney = "00000000" + player.money;			
		} else if (player.money < 100) {
			playerMoney = "0000000" + player.money;	
		} else if (player.money < 1000) {
			playerMoney = "000000" + player.money;	
		} else if (player.money < 10000) {
			playerMoney = "000000" + player.money;	
		} else if (player.money < 100000) {
			playerMoney = "00000" + player.money;	
		} else if (player.money < 1000000) {
			playerMoney = "0000" + player.money;	
		} else if (player.money < 10000000) {
			playerMoney = "000" + player.money;	
		} else if (player.money < 100000000) {
			playerMoney = "00" + player.money;	
		} else if (player.money < 1000000000) {
			playerMoney = "0" + player.money;
		} else {
			playerMoney = "" + player.money;
		}
		g.drawString(playerMoney, 5*15, 25);	
		
	
		
	
		// Every tenth second, collect money
		long now = System.currentTimeMillis();
		if (now - lastTime > 10000) {
			income = 0;
			for (Word word : player.word) {
				income += word.getIncome();		
			}
			player.money += income;
			lastTime = System.currentTimeMillis();
		}
		
		
		
		
		// Draw player income
		String drawIncome;
		if (income < 10) {
			drawIncome = "+ 000" + income;			
		} else if (income < 100) {
			drawIncome = "+ 00" + income;	
		} else if (income < 1000) {
			drawIncome = "+ 0" + income;	
		} else {
			drawIncome = "+ " + income;	
		}
		g.drawString(drawIncome, 19*15, 25);
		
		
		
		
		// Draw the word in the making
		wordInMaking = "";
		for(int k = 0; k <= currentCharNr; k++) {
			wordInMaking += player.letter[word[k]].letter;
		}
		
		i = 13*15 - wordInMaking.length()*4;
		g.drawString(wordInMaking, i, 150);
		
		
		
		
		// Prints created words
		// Checks if user can scroll left
		if (scrollStart > 0) {
			scrollLeft = true;
		} else {
			scrollLeft = false;
		}
		
		
		// Draws the words
		i = 50;
		for (int k = scrollStart; k < player.word.size(); k++) {
			player.word.get(k).drawInfo(g, i);
			
			// Continues past window to know if there are other letters to see to the right
			i += player.word.get(k).word.length()*15 + 30;
		}
		
		
		// Checks if user can scroll right
		if (i > WIDTH-25) {
			scrollRight = true;
		} else {
			scrollRight = false;
		}
		
	
		
		
	}

	
	
	
	
	
	

	public void keyTyped(KeyEvent e) {
		// NOT USED
	}

	public void keyReleased(KeyEvent e) {
		// NOT USED
	}
	
	
	
	
	
	
	
	
	// User input handling
	public void keyPressed(KeyEvent e) {
		
		
			// Scrolls through Letters up and down
		if (e.getKeyCode() == KeyEvent.VK_UP) {
			
			if (whichLetter == 0) {
				whichLetter = 25;		// Error handling/ scrolling forever
			} else {
				whichLetter--;			// Scrolls down
			}
			
		} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			
			if (whichLetter == 25) {
				whichLetter = 0;		// Error handling/ scrolling forever
			} else {
				whichLetter++;			// Scrolls down
			}
			
			
			
			
			// Buys the letter whichLetter is "pointing" to
		} else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
					
			if (whichLetter != 4) {
				userMessage = player.buyLetter(whichLetter);
			}
			
			
			
			
			// Scrolls thought the words, left or right
		} else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
			
			if(scrollLeft && scrollStart > 0) {
				scrollStart--;
			}
			
		} else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
			
			if(scrollRight) {
				scrollStart++;
			}
			
			
			
			
			// Entering/Adding the written word
		} else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			
			// Compiling the letters to one string
			wordInMaking = "";
			for(int i = 0; i <= currentCharNr; i++) {
				wordInMaking += player.letter[word[i]].letter;
			}
			

			// Error handling
			if(wordInMaking == "") {
				userMessage = "NO LETTERS WRITTEN";
				
			
			} else {
				
				// Put in: Checks the word is an actual word
				
				
				player.addWord(wordInMaking);
				
				currentCharNr = -1;
				
				
				// If new lenght give user money
				if(wordInMaking.length() > player.longestWord) {
					player.longestWord = wordInMaking.length();
					income = player.longestWord*10;
					player.money += income;
				} else {
					income = 0;
				}
			}
			
			
			
			
			// Removing a letter too not use it
		} else if (e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
			
			player.letter[word[currentCharNr--]].regretChar();
			
			
			
			
			// If user is writing letters
		} else if ((e.getKeyChar() >= 'a' && e.getKeyChar() <= 'z') || (e.getKeyChar() >= 'A' && e.getKeyChar() <= 'Z')) {
			
			// Error handling
			if (currentCharNr >= 19) {
				userMessage = "CAN'T ADD ANYMORE LETTERS";
				
				
			} else {
				// Securing the letters are uppercase
				char checkChar = Character.toUpperCase(e.getKeyChar());
				
				
				// Checks if user has at least one letter to use
				if(!player.letter[checkChar -65].checkIfChar()) {
					userMessage = "NO " + player.letter[checkChar -65].letter + " LEFT";
				} else {
					word[++currentCharNr] = checkChar - 65;
					
					
				}
			}
		}
	}


	
	
	
	
	
	
	// Running the game foreveeer
	public void run() {
		for(;;) {
			
			
			repaint();
			
			// To make sure the repaint and keyEvent doesn't go lightning speed
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	


}
