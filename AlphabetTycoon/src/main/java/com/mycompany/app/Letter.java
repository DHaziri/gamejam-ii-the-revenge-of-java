package com.mycompany.app;

public class Letter {

	String letter;
	int amount;
	int price;
	String infoPrint;
	
	
	
	
	// Recreates the string with the updated amount of letters
	public void updateString() {
		infoPrint = letter + " ";
		
		if (letter == "E") {
			infoPrint += "--      000";
		} else {
			
			if (amount < 10) {
				infoPrint += "0";
			}
			
			infoPrint += amount + "    -";
			
			if(price < 10) {
				infoPrint += "00";
			} else if (price < 100) {
				infoPrint += "0";
			}
			
			infoPrint += price;
		}
	}
	

	
	
	// Checks if the char is not zero
	public Boolean checkIfChar () {
		
		// Error handling
		if(amount == 0 && letter != "E") {
			return false;
		}
		
		amount--;
		return true;
	}
	
	
	
	
	// Readds  one to amount when user do not want to use letter
	public void regretChar() {
		amount++;
	}
	
	

}
