package com.mycompany.app;

import java.awt.Graphics;

public class Word {

	String word;
	int valuePrHit;
	int frecency;
	
	
	
	public Word (String w) {
		word = w;
		valuePrHit = word.length()*2;   //valuePrHit = 10 / WordsAPI.diversity(word);
		frecency = 21-word.length();  	// frecency = WordsAPI.perMillion // frecency = actual amount found of word on a webpage
		
	}
	
	
	
	// Draws the word and it's value
	public void drawInfo(Graphics g, int i) {
		
		g.drawString(word, i, 250);
		
		String temp = "" + valuePrHit;
		i += word.length()*4;
		
		g.drawString(temp, i, 275);
		
	}
	
	
	
	// Find how much income this word generates this moment
	public int getIncome() {
		
		double rand = (Math.random()*((100-1)+1))+1;
		int income = (int) (rand * frecency * valuePrHit);
		return income;
	}
	

	
	
}
