package com.mycompany.app;

import java.util.ArrayList;

//import java.io.File;
//import java.io.FileNotFoundException;
//import java.util.Scanner;

public class Player {

	int money;
	int longestWord;
	
	// List of what letter player has and what word they have created
	Letter [] letter = new Letter[26];
	ArrayList<Word> word = new ArrayList<Word>();
	
	
	public Player() {
		
		money = 70;
		longestWord = 0;
		
		
		
		// Initialzing letter array
		for (int i = 0; i < 26; i++) {
			letter[i] = new Letter();
		}
		
		
		// Lazy, bad, non readFromFile filling an array
		letter[0].letter = "A";
		letter[0].amount = 5;
		letter[0].price = 20;
		
		letter[1].letter = "B";
		letter[1].amount = 0;
		letter[1].price = 90;
		
		letter[2].letter = "C";
		letter[2].amount = 0;
		letter[2].price = 70;
		
		letter[3].letter = "D";
		letter[3].amount = 0;
		letter[3].price = 60;
		
		letter[4].letter = "E";
		letter[4].amount = 0;
		letter[4].price = 0;
		
		letter[5].letter = "F";
		letter[5].amount = 0;
		letter[5].price = 80;
		
		letter[6].letter = "G";
		letter[6].amount = 0;
		letter[6].price = 90;
		
		letter[7].letter = "H";
		letter[7].amount = 0;
		letter[7].price = 50;
		
		letter[8].letter = "I";
		letter[8].amount = 0;
		letter[8].price = 30;
		
		letter[9].letter = "J";
		letter[9].amount = 0;
		letter[9].price = 120;
		
		letter[10].letter = "K";
		letter[10].amount = 0;
		letter[10].price = 100;
		
		letter[11].letter = "L";
		letter[11].amount = 0;
		letter[11].price = 60;
		
		letter[12].letter = "M";
		letter[12].amount = 0;
		letter[12].price = 70;
		
		letter[13].letter = "N";
		letter[13].amount = 0;
		letter[13].price = 30;
		
		letter[14].letter = "O";
		letter[14].amount = 0;
		letter[14].price = 40;
		
		letter[15].letter = "P";
		letter[15].amount = 0;
		letter[15].price = 90;
		
		letter[16].letter = "Q";
		letter[16].amount = 0;
		letter[16].price = 110;
		
		letter[17].letter = "R";
		letter[17].amount = 0;
		letter[17].price = 50;
		
		letter[18].letter = "S";
		letter[18].amount = 0;
		letter[18].price = 40;
		
		letter[19].letter = "T";
		letter[19].amount = 5;
		letter[19].price = 10;
		
		letter[20].letter = "U";
		letter[20].amount = 0;
		letter[20].price = 70;
		
		letter[21].letter = "V";
		letter[21].amount = 0;
		letter[21].price = 90;
		
		letter[22].letter = "W";
		letter[22].amount = 0;
		letter[22].price = 80;
		
		letter[23].letter = "X";
		letter[23].amount = 0;
		letter[23].price = 130;
		
		letter[24].letter = "Y";
		letter[24].amount = 0;
		letter[24].price = 80;
		
		letter[25].letter = "Z";
		letter[25].amount = 0;
		letter[25].price = 140;
				
		
		
		
/*	File fileI = new File("Numbers.txt");
		Scanner sI;
		try {
			sI = new Scanner(fileI);
			
			money = sI.nextInt();
			longestWord = sI.nextInt();
			
			for (int i = 0; i < 26; i++) {
				letter[i].amount = sI.nextInt();
				letter[i].price = sI.nextInt();
			}
			
			sI.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		File file = new File("Letters.txt");
		Scanner s;
		try {
			s = new Scanner(file);
			
			for (int i = 0; i < 26; i++) {
				letter[i].letter = s.next();
			}
			
			s.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
	
	
	// Buys letters
	public String buyLetter(int l) {
		
		// Error handling
		if (money < letter[l].price) {
			return "NOT ENOUGH MONEY";
		} 
		
		
		money = money - letter[l].price;
		letter[l].amount++;
		return "LETTER BOUGHT";	
	}

	
	// Adds new word to the word array
	public void addWord(String w) {
		word.add(new Word(w));
	
	}
	
	
	
}
